//======================================================================
// audio.h
//
// NOTE:  Let's change the name of this file to RigAudio.h.
//======================================================================

#ifndef __iop_audio_h__
#define __iop_audio_h__

#include <Audio.h>
#include <dynamicFilters.h>
#include <effect_compressor_fb.h>
#include "config.h"

class RigAudio 
{
  public:
  RigAudio(AudioConfig& c): _config(c) {}

  void init() const;

  void muteRx() const;
  void unmuteRx() const;

  void muteAllTx() const;

  void muteMicIn() const;
  void unmuteMicIn() const;

  void muteLineIn() const;
  void unmuteLineIn() const;

  void muteUSBIn() const;
  void unmuteUSBIn() const;

  void muteTTIn() const;
  void unmuteTTIn() const;

  void muteSpkrOut() const;
  void unmuteSpkrOut() const;

  void muteLineOut() const;
  void unmuteLineOut() const;
  
  void muteUSBOut() const;
  void unmuteUSBOut() const;

  private:
  AudioConfig _config;
};

//======================================================================

class bp_filter {
  
  public:
  //bp_filter(double f1, double f2, bool use_center, short int window=-1, short int coeff=-1);
  bp_filter();
  bp_filter(AudioFilterFIR& f, AudioAmplifier& a);
  void init(const bpf_config& cfg);
  //void init(AudioFilterFIR* filter=NULL, short* coefficients=NULL);
  void set_band(double f1, double f2);
  void set_freq_lo(double f);
  void set_freq_hi(double f);
  void set_center_and_width(double c, double w);
  void set_center(double c);
  void set_width(double w);
  void set_gain(double g);
  void enable();
  void disable();

  private:
  
  double freq_lo;
  double freq_hi;
  short int window;
  short int coeff;
  float recovery;          // recovery amplifier value

  AudioFilterFIR& filter; // = &filterRX;
  AudioAmplifier& amp;
  short coefficients[NUM_COEFFICIENTS];
  bool setup_complete;
};

//======================================================================

class speech_comp
{
  public:
  speech_comp(comp_config* cfg);
//  speech_comp(AudioEffectCompressor&, AudioAmplifier&, AudioAnalyzeRMS&);
  void update();
  void enable();
  void disable();
  inline bool is_enabled() const { return config_->enabled; }

  private:
  comp_config* config_;
  AudioEffectCompressor& comp_;
  AudioAmplifier& amp_;
  AudioAnalyzeRMS& rms_;
  float env_ = 1.0;
  float alpha_ = 0.8;
};

//======================================================================

/*
void audioInit();
void audioSelectTxInput(TxInput);
void audioTransmit();
void audioReceive();
void audioCalibrate(AudioConfig *, char, char, char, float, bool);
*/

#endif

//======================================================================
// EOF
//======================================================================
