//======================================================================
// ubitx_iop.h
//======================================================================

#ifndef __ubitx_iop_h__
#define __ubitx_iop_h__

#include "config.h"
#include "audio.h"
#include "cat.h"
#include "eeprom.h"
#include "keyer.h"

#define PTT_KEY_OUT_PIN 2
#define ENCODER_A_PIN   5
#define ENCODER_B_PIN   4
#define ENCODER_SW_PIN  3

// comment this out to disable debugging code
#define DEBUG

#if defined(DEBUG)
#define USBDEBUG(x) USBSERIAL.print("IOP: "); USBSERIAL.println(x);
#else
#define USBDEBUG(x)
#endif

//enum RigMode {
//  MODE_SSB = 0,
//  MODE_DIGI = 1,
//  MODE_CW = 2,
//};

enum TxState {
  TX_OFF = 0,
  TX_MIC,
  TX_LINE,
  TX_CAT,
  TX_KEYER,
};

//extern RigMode rigMode;

extern bool keyerKeyDown;

//======================================================================
// Keying functions.
//
// These are simple functions to assert the key line from the IOP to the
// Raduino.
//======================================================================

inline void initKeyLine() 
{
  pinMode(PTT_KEY_OUT_PIN, OUTPUT);
  digitalWrite(PTT_KEY_OUT_PIN, HIGH);  
}

inline void setKeyDown() 
{
  digitalWrite(PTT_KEY_OUT_PIN, LOW);
}

inline void setKeyUp() 
{
  digitalWrite(PTT_KEY_OUT_PIN, HIGH);
}

//======================================================================

#endif

//======================================================================
// EOF
//======================================================================
