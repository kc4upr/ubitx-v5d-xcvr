//======================================================================
// RigMode.h
//======================================================================

#ifndef __RigMode_h__
#define __RigMode_h__

#include "audio.h"

//======================================================================
// basic_mode
//
// A basic rig mode.  It can perform actions on_entry() and on_exit(),
// as well as on_tx() and on_rx().  These actions are defined in an 
// appropriate subclass.  In addition, the basic mode support an audio
// audio band pass filter with wide, medium, and narrow modes.
//======================================================================

class basic_mode {
  public:
  
  basic_mode(mode_config& c, RigAudio& a, bp_filter& f) :
  config_(c), audio_(a), filter_(f), active_(false), transmitting_(false) 
  {
  }
  
  virtual ~basic_mode() {}

  inline mode_config& config() { return config_; }

  inline RigAudio& audio() { return audio_; }

  inline bp_filter& filter() { return filter_; }

  // Called upon mode entry.  Should assume that the rig's state is 
  // "clean", i.e. that it still needs to enable anything it needs for
  // use in the mode.
  virtual void on_entry() = 0;

  // Called upon mode exit.  Should clean everything up that it used,
  // and not make assumptions about which mode is being entered next.
  virtual void on_exit() = 0;

  // Called when transmitting.
  virtual void on_tx() = 0;

  // Called when receiving.
  virtual void on_rx() = 0;

  inline void enter() {
    if (active_) {
      // Do nothing.  Exceptions not active (for Arduino, I assume?).
      //throw MODE_ALREADY_ACTIVE;
      USBDEBUG("ERROR - tried to enter mode, but it's already active");
    } else {
      active_ = true;
      on_entry();
    }
  }

  inline void exit() {
    if (transmitting_) {
      // Do nothing.  Exceptions not active (for Arduino, I assume?).
      //throw MODE_STILL_TRANSMITTING;
      USBDEBUG("ERROR - tried to exit mode, but it's transmitting");
    } else {
      active_ = false;
      on_exit();
    }
  }

  inline bool is_active() { return active_; }
  inline bool is_inactive() { return !active_; }

  inline void tx() {
    if (transmitting_) {
      // Do nothing.  Exceptions not active (for Arduino, I assume?).
      //throw MODE_ALREADY_TRANSMITTING;
      USBDEBUG("ERROR - tried to start transmitting, but mode already is transmitting");
    } else {
      transmitting_ = true;
      on_tx();
    }
  }

  inline void rx() {
    if (!transmitting_) {
      // Do nothing.  Exceptions not active (for Arduino, I assume?).
      //throw MODE_NOT_TRANSMITTING;
      USBDEBUG("ERROR - tried to start receiving, but mode already is receiving");
    } else {
      transmitting_ = false;
      on_rx();
    }
  }

  inline bool is_tx() const { return transmitting_; }
  inline bool is_rx() const { return !transmitting_; }

  inline void set_rx_filter(rx_filter f) {
    filter_.disable();
    config_.filter = f;
    filter_.init(config_.filter_cfg[static_cast<int>(config_.filter)]);
    filter_.enable();
    #if defined(DEBUG)
    switch(config_.filter) {
      case rx_filter::wide:
      USBDEBUG("selected wide filter");
      break;

      case rx_filter::medium:
      USBDEBUG("selected medium filter");
      break;

      case rx_filter::narrow:
      USBDEBUG("selected narrow filter");
      break;
    }
    #endif
  }
  
  private:
  
  mode_config& config_;
  RigAudio& audio_;
  bp_filter& filter_;
  bool active_;
  bool transmitting_;
};

//======================================================================
// ssb_mode
//======================================================================

class ssb_mode : public basic_mode
{
  public:

  ssb_mode(ssb_config& c, RigAudio& a, bp_filter& f, speech_comp& s, bool default_mic=true) : 
  basic_mode(c, a, f), use_mic_(default_mic), comp_(s) {}
      
  virtual void on_entry() 
  {
    set_rx_filter(config().filter);
    if (comp_.is_enabled()) {
      enable_comp();
    }
    audio().unmuteRx();
    audio().muteAllTx();
    USBDEBUG("SSB mode entered");
  }
  
  virtual void on_exit() {    
    audio().muteAllTx();
    audio().muteRx();
    disable_comp();
    USBDEBUG("SSB mode exited");
  }
  
  virtual void on_tx() 
  {
    audio().muteRx();    
    if (use_mic_) {
      audio().unmuteMicIn();
    } else {
      audio().unmuteLineIn();      
    }
    USBDEBUG("SSB mode transmitting");
  }
  
  virtual void on_rx() 
  {    
    if (use_mic_) {
      audio().muteMicIn();
    } else {
      audio().muteLineIn();
    }
    audio().unmuteRx();
    USBDEBUG("SSB mode receiving");
  }

  void set_mic_in() 
  {
    if (is_rx()) {
      // can't switch inputs while already transmitting
      use_mic_ = true;
    }
    USBDEBUG("SSB mode - Mic In set");
  }

  void set_line_in() 
  {
    if (is_rx()) {
      // can't switch inputs while already transmitting
      use_mic_ = false;
    }
    USBDEBUG("SSB mode - Line In set");
  }

  inline void enable_comp()  { comp_.enable(); }
  inline void disable_comp() { comp_.disable(); }

  private:

  bool use_mic_;
  speech_comp& comp_;
};

//======================================================================
// digi_mode
//======================================================================

class digi_mode : public basic_mode 
{
  public:

  digi_mode(digi_config& c, RigAudio& a, bp_filter& f) :
  basic_mode(c, a, f) {}

  virtual void on_entry() 
  {
    set_rx_filter(config().filter);
    audio().unmuteRx();
    audio().muteAllTx();
    USBDEBUG("Digi mode entered");
  }
  
  virtual void on_exit() {    
    audio().muteAllTx();
    audio().muteRx();
    USBDEBUG("Digi mode exited");
  }
  
  virtual void on_tx() 
  {
    audio().muteRx();
    audio().unmuteUSBIn();
    USBDEBUG("Digi mode transmitting");
  }
  
  virtual void on_rx() 
  {    
    audio().muteUSBIn();
    audio().unmuteRx();
    USBDEBUG("Digi mode receiving");
  }
};


//======================================================================
// cw_mode
//======================================================================

class cw_mode : public basic_mode
{
  public:

  cw_mode(cw_config& c, RigAudio& a, bp_filter& f):
  basic_mode(c, a, f) {}

  virtual void on_entry() 
  {
    set_rx_filter(config().filter);
    audio().unmuteRx();
    audio().muteAllTx();
    USBDEBUG("CW mode entered");
  }
  
  virtual void on_exit() {    
    audio().muteAllTx();
    audio().muteRx();
    USBDEBUG("CW mode exited");
  }
  
  virtual void on_tx() 
  {
    // Currently not muting Rx, since the uBITX produces it's own
    // sidetone... but I'm probably going to replace that with a S/W-
    // generated sidetone.
    USBDEBUG("CW mode transmitting");
  }
  
  virtual void on_rx() 
  {    
    USBDEBUG("CW mode receiving");
  }
};

#endif

//======================================================================
// EOF
//======================================================================
