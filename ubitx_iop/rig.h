//======================================================================
// rig.h
//======================================================================

#ifndef __rig_h__
#define __rig_h__

#include <iopcomm.h>
#include "audio.h"
#include "RigMode.h"

//======================================================================
// basic_rig
//======================================================================

class basic_rig {
  public:

  //--------------------------------------------------------------------
  // Constructor/destructor.
  //--------------------------------------------------------------------

  basic_rig(RigConfig& c, RigAudio& a) : 
    config_(c), audio_(a), 
    bpf_(), 
    comp_(&config_.ssb.comp),
    ssb_ (config_.ssb,  audio_, bpf_, comp_),
    cw_  (config_.cw,   audio_, bpf_),
    digi_(config_.digi, audio_, bpf_) { set_rig_mode(config_.mode); }

  void init() {}

  RigConfig& config() { return config_; }
  
  //--------------------------------------------------------------------
  // Mode control.
  //--------------------------------------------------------------------

  inline bool is_ssb_mode()  const { return (config_.mode == rig_mode::ssb ); }
  inline bool is_digi_mode() const { return (config_.mode == rig_mode::digi); }
  inline bool is_cw_mode()   const { return (config_.mode == rig_mode::cw  ); }

  // Switch to the specified mode.  Returns a pointer to the new mode.
  basic_mode* set_rig_mode(rig_mode m) {
    // exit the previous mode
    current_mode_->exit(); // NOTE:  This could currently occur during TX, which is NOT desirable.

    // select the new mode
    config_.mode = m;

    switch(config_.mode) {
      case rig_mode::ssb:
      current_mode_ = &ssb_;
      break;

      case rig_mode::cw:
      current_mode_ = &cw_;
      break;

      case rig_mode::digi:
      current_mode_ = &digi_;
      break;
    }

    //enter the new mode
    current_mode_->enter();

    // return a pointer to the new mode
    return current_mode_;
  }

  // Returns the rig_mode (enum) of the current mode object.
  inline rig_mode get_rig_mode() const { return config_.mode; }


  // Returns a pointer to the current mode object.
  inline basic_mode* mode() const { return current_mode_; }

  //--------------------------------------------------------------------
  // Transmit/Receive (T/R) control.
  //--------------------------------------------------------------------

  inline bool is_tx() const { return mode()->is_tx(); }
  inline bool is_rx() const { return mode()->is_rx(); }

  // Enter the transmit state.  This is delegated to the mode.
  inline void tx() { mode()->tx(); }

  // Enter the receive state.  This is delegated to the mode.
  inline void rx() { mode()->rx(); }

  //--------------------------------------------------------------------
  // Transmit processor control.
  //--------------------------------------------------------------------

  inline void enable_comp() {
    if (is_ssb_mode()) ((ssb_mode*)mode())->enable_comp();    
  }

  inline void disable_comp() {
    if (is_ssb_mode()) ((ssb_mode*)mode())->disable_comp();        
  }

  //--------------------------------------------------------------------
  // RX filter control.
  //--------------------------------------------------------------------

  // Set the RX filter.  This is delegated to the mode.
  inline void set_rx_filter(rx_filter f) { mode()->set_rx_filter(f); }

  // Returns the rx_filter (enum) of the RX filter currently being used.
  inline rx_filter get_rx_filter() const { return mode()->config().filter; }

  void next_rx_filter() {
    switch(mode()->config().filter) {
      case rx_filter::wide:
      set_rx_filter(rx_filter::medium);
      break;

      case rx_filter::medium:
      set_rx_filter(rx_filter::narrow);
      break;

      case rx_filter::narrow:
      set_rx_filter(rx_filter::wide);
      break;
    }
  }

  void prev_rx_filter() {
    switch(mode()->config().filter) {
      case rx_filter::wide:
      set_rx_filter(rx_filter::narrow);
      break;

      case rx_filter::medium:
      set_rx_filter(rx_filter::wide);
      break;

      case rx_filter::narrow:
      set_rx_filter(rx_filter::medium);
      break;
    }
  }

  //--------------------------------------------------------------------
  // Audio output control.
  //--------------------------------------------------------------------

  inline void muteSpkrOut() const {
    audio_.muteSpkrOut();
  }

  inline void unmuteSpkrOut() const {
    audio_.unmuteSpkrOut();
  }

  inline void muteLineOut() const {
    audio_.muteLineOut();
  }

  inline void unmuteLineOut() const {
    audio_.unmuteLineOut();
  }

  inline void muteUSBOut() const {
    audio_.muteUSBOut();
  }

  inline void unmuteUSBOut() const {
    audio_.unmuteUSBOut();    
  }

  //--------------------------------------------------------------------
  // Audio input control.
  //--------------------------------------------------------------------

  inline void muteAllTx() const {
    audio_.muteAllTx();
  }

  inline void muteMicIn() const {
    audio_.muteMicIn();
  }

  inline void unmuteMicIn() const {
    audio_.unmuteMicIn();
  }

  inline void muteLineIn() const {
    audio_.muteLineIn();
  }

  inline void unmuteLineIn() const {
    audio_.unmuteLineIn();
  }

  inline void muteUSBIn() const {
    audio_.muteUSBIn();
  }

  inline void unmuteUSBIn() const {
    audio_.unmuteUSBOut();    
  }

  inline void muteTTIn() const {
    audio_.muteTTIn();
  }

  inline void unmuteTTIn() const {
    audio_.unmuteTTIn();    
  }

  // Update the rig state.  This should be called once each time through
  // the main loop.
  void update() 
  {
    comp_.update();   // It checks if it's enabled on its own.
  }
  
  private:

  RigConfig& config_;
  RigAudio&  audio_;

  bp_filter bpf_;
  speech_comp comp_;

  ssb_mode  ssb_;
  cw_mode   cw_;
  digi_mode digi_;

  basic_mode* current_mode_;
};

#endif

//======================================================================
// EOF
//======================================================================
